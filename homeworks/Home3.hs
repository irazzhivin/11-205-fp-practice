
-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo 0 = 0
tribo 1 = 1
tribo 2 = 2
tribo 3 = 3
tribo n = tribo (n-1) + tribo (n-2) + tribo (n-3)



data Color = Color
			| White
			| Black
			| Red
			| Green
			| Blue
			| Mix Int Int Int 
			deriving (Show)

sumColors :: Color -> Color -> Color
sumColors White _ = White
sumColors _ White = White
sumColors Black a = a
sumColors a Black = a
sumColors Red Red = Red
sumColors Red Green = Mix 255 255 0
sumColors Red Blue = Mix 255 0 255
sumColors Red (Mix _ b c) = Mix 255 b c
sumColors (Mix _ b c) Red = Mix 255 b c
sumColors Green Green = Green
sumColors Green Red = Mix 255 255 0
sumColors Green Blue = Mix 0 255 255
sumColors Green (Mix a _ c) = Mix a 255 c
sumColors (Mix a _ c) Green = Mix a 255 c
sumColors Blue Blue = Blue
sumColors Blue Red = Mix 255 0 255
sumColors Blue Green = Mix 0 255 255
sumColors Blue (Mix a b _) = Mix a b 255
sumColors (Mix a b _) Blue = Mix a b 255
sumColors (Mix x y z) (Mix a b c) = Mix (if ((a + x) > 255) then 255 else (a + x)) (if ((b + y) > 255) then 255 else (b + y)) (if((c + z) > 255) then 255 else (c + z))

getRedChannel :: Color -> Int
getRedChannel White = 255
getRedChannel Red = 255
getRedChannel (Mix a _ _) = a
getRedChannel _ = 0

getGreenChannel :: Color->Int
getGreenChannel White = 255
getGreenChannel Green = 255
getGreenChannel (Mix a b c) = b
getGreenChannel _ = 0

getBlueChannel :: Color->Int
getBlueChannel White = 255
getBlueChannel Blue = 255
getBlueChannel (Mix a b c) = c
getBlueChannel _ = 0