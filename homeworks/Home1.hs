pi1 n = 4 / (1 + fn_1 1 n)
fn_1 i n = if i <= n then ((i * i) / (2 + fn_1 (i+2) n)) else 1

pi2 n = 3 + fn_2 1 n
fn_2 i n = if i <= n then ((i * i) / (6 + fn_2 (i+2) n)) else 1

pi3 n = 4 / fn_3 1 n
fn_3 i n = if i <= n then ((i * 2 -1) + i * i / fn_3 (i+1) n) else 1

pi4 n = test n
test n = if n > 0 then (if mod (truncate(n)) 2 /= 0 then (if mod (truncate(n+1)) 4 == 0 then -(4 / n - test(n-2)) else (4 / n + test(n-2))) else test(n-1)) else 0

pi5 n = 3 + fn_5 2 n
fn_5 i n = if i <= n then (if mod (truncate(i)) 4 == 0 then -(4 / (i * (i + 1) * (i + 2)) - fn_5(i+2) n) else (4 / (i * (i + 1) * (i + 2)) + fn_5(i+2) n) ) else 0