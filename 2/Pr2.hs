
{-
Int
Double
Bool

Integer

String = [Char]
-}

test :: Int -> Int
test x = x

len1 :: [a] -> Int
len1 xs = if null xs -- null returns whether list is empty
          then 0
          else 1 + len1 (tail xs) -- tail returns tail of list

len2 :: [a] -> Int
len2 xs = case xs of
            [] -> 0
            (y:ys) -> 1 + len2 ys

len3 :: [a] -> Int
len3 [] = 0
len3 (x:xs) = 1 + len3 xs

sumByTwo :: [Int] -> [Int]              
sumByTwo [] = []
sumByTwo [x] = [x]
sumByTwo (x:y:xs) = (x+y) : sumByTwo xs

{- group elements:
myGroup [1,1,2,1,3,2] = [[1,1],[2],[1],[3],[2]]

group
-}
myGroup :: [Int] -> [[Int]]
myGroup [] = []
myGroup (x:xs) = repl (1 + k) : myGroup (dropFirst k xs)
    where k = count x xs

          count x [] = 0
          count x (y:xs) | x==y      = 1 + count x xs
                         | otherwise = 0

          -- replicate
          -- in where clause you can use bindings from function definition
          repl k | k>0 = x : repl (k-1)
                 | otherwise = []

          -- drop
          dropFirst k [] = []
          dropFirst k (x:xs) | k>0 = dropFirst (k-1) xs
                             | otherwise = x:xs

myGroup' :: [Int] -> [[Int]]
myGroup' [] = []
myGroup' (x:xs) = let
    k = count x xs

    count :: Int -> [Int] -> Int
    count x [] = 0
    count x (y:ys) | x == y    = 1 + count x ys
                   | otherwise = 0
 in replicate (1 + k) x : myGroup (drop k xs)

{-
  myWords "asd asdewr   qwesad" = ["asd","asdewr","qwesad"]
-}
myWords [] = []
myWords xs = takeWord withoutLeadingSpaces : myWords (myDropWhile isNotSpace withoutLeadingSpaces)
    where withoutLeadingSpaces = myDropWhile isSpace xs

          isSpace x = x == ' '
          isNotSpace x = x /= ' '

          myDropWhile :: (a -> Bool) -> [a] -> [a]
          myDropWhile f [] = []
          myDropWhile f (x:xs) | f x = myDropWhile f xs
                               | not (f x) = x : xs
                               | otherwise = x : myDropWhile f xs

          takeWord [] = []
          takeWord (' ':xs) = []
          takeWord (x:xs) = x : takeWord xs
