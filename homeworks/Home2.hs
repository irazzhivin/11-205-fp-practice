
-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt _ [] = []
deleteInt a (x:xs) = if (a /= x) then x : deleteInt a xs else deleteInt a xs  
             
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt a b = func 0 a b 
    where func i n [] = []
          func i n (x:xs) = if x/=n
                                then func (i + 1) n xs
                                else i : func (i + 1) n xs