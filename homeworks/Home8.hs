-- ������� ������������� �������� ������ �����
-- (���������� ������������� ������ �����, ��
-- ������ ������������� ������ -- ����� ���������)
decodeBinary :: String -> Integer
decodeBinary str = decodeBinary' 0 str where
	decodeBinary' num [] = num
	decodeBinary' num ('0':xs) = decodeBinary' (2*num) xs
	decodeBinary' num ('1':xs) = decodeBinary' (2*num+1) xs	

-- ������� ������������� ������ ����� � �������
-- ���������: ������� -- ����� ���������, ���
-- ���� ������ ������:
--    0f = 0
--    1f = 1
--   10f = 2
--  100f = 3
--  101f = 4
-- 1000f = 5
-- 1001f = 6
-- 1010f = 7
--   .....
-- (���������� ������������� ������ �����, ��
-- ������ ������������� ������ -- ����� ���������)
decodeFibo :: String -> Integer
decodeFibo str = decodeFibo' 0 1 (reverse str) where
	decodeFibo' prev curr ('1':xs) = prev + curr + decodeFibo' curr (prev + curr) xs
	decodeFibo' prev curr ('0':xs) = decodeFibo' curr (prev + curr) xs          
	decodeFibo' _ _ [] = 0
