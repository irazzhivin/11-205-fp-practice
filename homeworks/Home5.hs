﻿myMaximum :: [Int] -> Int
myMaximum [x] = x
myMaximum (x:xs) | x > t = x 
                 | otherwise = t
                 where t = myMaximum xs        

-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex l = 
   let pmax :: [Integer] -> Int -> (Int, Integer)
       pmax [x] xi = (xi, x)
       pmax (x:xs) xi | x > t = (xi, x)
                      | otherwise = (ti, t)
                      where (ti, t) = pmax xs (xi + 1)
   in mmax l 0

-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Int] -> Int
maxCount [x] = 1
maxCount (x:xs) | x == t = 1 + maxCount xs
                | otherwise = maxCount xs
                where t = myMaximum (x:xs) 
