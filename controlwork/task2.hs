data BiTree a = Empty | Node (BiTree a) a (BiTree a)




height :: BiTree a -> Int

height Empty = -1

height (Node left _ rright) = 1 + max (height left) (height right)



tmap (a -> b) -> BiTree a -> BiTree b

tmap _ Empty = Empty

tmap f (Node left e right) = Node (tmap f left) (f e) (tmap f right)

